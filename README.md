# OpenML dataset: Dril-Tweets

https://www.openml.org/d/43521

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Scraped from https://twitter.com/dril January 2020.
Content
Dataset contains date of tweet, text of tweet, and other statistics such as likes and retweets.
Acknowledgements
Many thanks to GetOldTweets for providing an excellent scraper.
Inspiration
age 0 (baby): I want my Dada . age 25 (Millennial): I want my Data Do you see how scuffed this is?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43521) of an [OpenML dataset](https://www.openml.org/d/43521). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43521/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43521/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43521/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

